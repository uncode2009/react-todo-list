import React from 'react';
import './header.scss';

const Header = (props) => {
    return (
        <div className="header">
          <div>
            <h2>Thursday, <span>10th</span></h2>
            <p className='month'>December</p>
          </div>
          <div className='tasksCount'>
              <span>{props.count}</span> Tasks
          </div>
        </div>
    )
}

export default Header;