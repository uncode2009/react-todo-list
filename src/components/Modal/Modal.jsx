import React from 'react';
import { useState } from 'react';
import './Modal.scss';

const Modal = (props) => {

    const [newTask, setNewTask] = useState('');
    const [hours, setHours] = useState('00');
    const [minutes, setMinutes] = useState('00');
    const [meridiem, setMeridiem] = useState('AM');

    const close = () => {
        props.setShowModal(false);
    }

    const addTask = e => {
        e.preventDefault();
        props.addTask({
            task: newTask,
            time: `${hours}:${minutes} ${meridiem}`,
          });
    }

    const createNewTask = e => {
        setNewTask(e.target.value);
    }

    const spesifyHours = e => {
        let time = e.target.value;
        time = time < 10 ? `0${time}` : '' + time;
        setHours(time);
    }

    const spesifyMinutes = e => {
        let time = e.target.value;
        time = time < 10 ? `0${time}` : '' + time;
        setMinutes(time);
    }

    const spesifyMeridiem = e => {
        setMeridiem(e.target.value);
    }

    return (
        <div className="modal" onClick={close}>
            <div className="modal_content" onClick={e => e.stopPropagation()}>
                <h3>
                    Add new task
                </h3>
                <form className='modal_form'> 
                    <input className="modal_input" placeholder="Enter new task" onChange={createNewTask} value={newTask} required/>
                    <div className="modal_time">
                        <p>Spesify time: </p>
                        <input type='number' className="modal_input" placeholder="00" onChange={spesifyHours} value={hours} min={0} max={12} required/>
                        <p> : </p>
                        <input type='number' className="modal_input" placeholder="00" onChange={spesifyMinutes} value={minutes} min={0} max={60} required/>
                        <select className="modal_input" name="meridiem" onChange={spesifyMeridiem} value={meridiem}>
                            <option value="AM">AM</option>
                            <option value="PM">PM</option>
                        </select>                    
                    </div>
                    <div className="modal_buttons">
                        <button onClick={close}>Cancel</button>
                        <button onClick={addTask}>Add</button>                    
                    </div>
                </form>
            </div>
            
        </div>
    )
}

export default Modal;