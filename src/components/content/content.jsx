import React from 'react';
import './content.scss';
import TaskItem from './Item/Item';

const Content = (props) => {
    const tasks = props.tasks.map(task => <TaskItem key={task._id} task={task} togleActiveStatus={props.togleActiveStatus} deleteTask={props.deleteTask} />);
    return (
        <div className="content">
            <ul>
                {tasks}
            </ul>

        </div>
    )
}

export default Content;