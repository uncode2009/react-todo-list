import './Item.scss';

const TaskItem = (props) => {

    return (
    <li className='taskBox'>
        <div className="taskItem">
            <input type="checkbox" onChange={() => (props.togleActiveStatus(props.task._id))} checked={!props.task.active}/>
            <p className={props.task.active ? 'task': 'task done'} onDoubleClick={() => props.deleteTask(props.task._id)}>{props.task.task}</p>
            <p>{props.task.time}</p>    
        </div>

    </li>
    )
}

export default TaskItem;