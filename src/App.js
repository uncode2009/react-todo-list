import { useState, useEffect } from 'react';
import './App.scss';
import Content from './components/content/content';
import Header from './components/header/header';
import Modal from './components/Modal/Modal';
import { api } from '../src/api/api'

function App() {
  const [tasks, setTasks] = useState([]);
  const [showModal, setShowModal] = useState(false);

  const fetchData = async () => {
    const data = await api.getTasks();
    setTasks(data);
  };

  useEffect(() => {
    fetchData().catch(console.error);
  }, []);

  const addTask = async (newTask) => {
    const result = await api.createTask(newTask);
    if (result) {
      setTasks([...tasks, result]);
      setShowModal(false);
    }
  };

  const togleActiveStatus = async (id) => {
    const currentTask = tasks.find(task => task._id === id);
    currentTask.active = !currentTask.active;
    const result = await api.updateTask(id, currentTask);
    if (result === 204) {
      let newTasks = [...tasks.map(task => task = task._id === id ? currentTask : task)];
      setTasks(newTasks);
    }
  };

  const deleteTask = async (id) => {
    const result = await api.deleteTask(id);
    if (result === 204){
      setTasks([...tasks.filter(task => task._id !== id)]);
    }
  };

  return (
    <div className="App">
      <div className="container">
        <Header count={tasks.length} />
        <button className="btn" onClick={() => setShowModal(true)}>+</button>
        {tasks.length >= 1 ? <Content tasks={tasks} togleActiveStatus={togleActiveStatus} deleteTask={deleteTask} /> : <p>Your ToDo list is empty. Create first task!</p>}
        
        {showModal && <Modal setShowModal={setShowModal} addTask={addTask}/>}
      </div>
    </div>
  );
}

export default App;
