import axios from "axios";

const instance = axios.create({
    baseURL: 'https://back-for-react-app.herokuapp.com',
    headers: {
        'Access-Control-Allow-Origin': 'true'
    }
});

export const api = {
    getTasks() {
        return instance.get(`/tasks`).then(response => response.data);
    },
    createTask(newTask) {
        return instance.post('/tasks', newTask).then(response => response.data)
    },
    updateTask(id, task) {
        return instance.patch('/tasks/' + id, task).then(response => response.status)
    },
    deleteTask(id) {
        return instance.delete('/tasks/' + id).then(response => response.status);
    }
};
